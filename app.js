/**
@author: amassillo
**/

const config = require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const expressValidator = require('express-validator')
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const axios = require('axios');



  
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(expressValidator());


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.listen(3000, err => {
	if (err) {
		logger.error(err);
		process.exit(1);
	}

	require('./utils/db');
});

module.exports = app;
