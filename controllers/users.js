/**
@author: amassillo
**/
//models----------------------------------------------------
const User 	  = require('../models/user');
const Calendar = require('../models/calendar');
const Event 	  = require('../models/event');

//validations, password encryption
const { validationResult } = require('express-validator/check');
const axios = require('axios');
const crypto = require('crypto');
 
exports.getUser = async (req, res) => {
    var userId = req.params.id;
	try{
		let user  = await User.findOne({ 'uname': userId });
		res.send(user);
	}catch(err){
		res.status(400).send(err);
	}	
}

exports.saveUser = async  (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({ errors: errors.array() });
	}

	var lUser = new User();
	lUser.fname = req.body.fname;
	lUser.lname = req.body.lname;
	lUser.uname = req.body.username;
	
	try{
		let doc =  await lUser.save();
		res.send("OK");
	}catch(err){
		console.error(err);
		res.status(400).send("not OK");
	}
}

exports.saveCalendar = async  (req, res) => {
	var lCalendar = new  Calendar();
	lCalendar.created_by =  req.params.id;
		
	try{
		let doc =  await lCalendar.save();
		res.send("OK");
	}catch(err){
		console.error(err);
		res.status(400).send("not OK");
	}
}

exports.saveEvent = async  (req, res) => {
	if  (!req.params.calendarId)
		return res.status(400).send("not OK");
	var lEvent = new Event();
	lEvent.title = req.body.title;
	lEvent.description = req.body.description;
	lEvent.meeting_link = req.body.link;
	lEvent.start_d = req.body.start_d;
	lEvent.duration = req.body.duration;
	lEvent.invites = req.body.invites;
	
	try{
		let doc =  await  Calendar.update(
													{ _id: req.params.CalendarId }, 
													{ $push: { events: lEvent } }
												);
		console.log(doc);
		res.send("OK");
	}catch(err){
		console.error(err);
		res.status(400).send("not OK");
	}
}

exports.getUserCalendars = async  (req, res) => {
	 var userId = req.params.id;
	 var lday = req.params.day;
	try{
		let user  = await Calendar.find({ 'created_by': userId })
													.populate({
															path: 'events.invites',
															select: 'fname lname email',
															model: 'User'
													})
											.exec();
									
		res.send(user);
	}catch(err){
		res.status(400).send(err);
	}	
}

exports.postCommentOnEvent = async  (req, res) => {
	 var CalendarId = req.params.calendarId;
	 var eventId = req.params.eventId;
	 var userId 	 = req.params.id;
	 var comment = req.params.comment;
	 console.log(eventId);
	try{
		let userComment = {body : comment, userId};
		/*let doc =  await  Calendar.update(
												//	{ _id: req.params.calendarId }, 
													{ events._id: req.params.eventId }, 
													{ $push: { events.$.comments: userComment } }
												);*/
									
		let doc =  await  Calendar.update({'events._id': req.params.eventId}, 
															  {$push: {'$.comments': userComment}})
		res.send(doc);
	}catch(err){
		console.log(err);
		res.status(400).send(err);
	}	
}

exports.getAsyncExample = async (req, res) => {
		try{
			let res = await axios.get('https://api.github.com/users/KrunalLathiya');
			console.log(res.data.login);
		}catch(err){
			console.log(err);
		}	
		res.send("OK");
}