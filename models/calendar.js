/**
@author: amassillo
**/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eventModel = require('./event');
const EventSchema = mongoose.model('Event').schema;

const CalendarSchema = new Schema({
	events: [EventSchema],
	created_by: 	 {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	last_updated_by: 	 {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	created_date: { type: Date,
							   default: Date.now },
	last_update_date: { type: Date},
});


module.exports = mongoose.model('Calendar', CalendarSchema);
