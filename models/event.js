/**

@author: amassillo

**/

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const {WhenTypeEnum} = require('./reminder');
const ReminderSchema = mongoose.model('Reminder').schema;

const EventSchema = new Schema({
	title: String,
	description: String,
    start_d: { type: Date},
    duration: Number,
	durationType:  { type: String,
								enum: Object.values(WhenTypeEnum)
								},
    invites: [{inviteStatus: Boolean, type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
	meeting_link: String,
	reminders: [ReminderSchema],
	comments: [{ body: String, date: Date, type: mongoose.Schema.Types.ObjectId, ref: 'User'  }],
});

module.exports = mongoose.model('Event', EventSchema);
