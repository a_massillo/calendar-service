/**
@author: amassillo
**/

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const WhenTypeEnum = Object.freeze({
	minutes: 'minutes',
	hrs: 'hours',
	days: 'days',
	weeks: 'weeks'
});

const ReminderSchema = new Schema({
	type: String,
	when: Number,
	whenType: {
						type: String,
						enum: Object.values(WhenTypeEnum)
						},
});

Object.assign(ReminderSchema.statics, {
  WhenTypeEnum,
});

module.exports = mongoose.model('Reminder', ReminderSchema);