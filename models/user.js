const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    fname: String,
    lname: String,
    uname: { type: String,
					index: true,
					unique: true
					},
    email: { type: String,
				 index: true
				},
    password: String,
    role: String, 
    image: String
});

module.exports = mongoose.model('User', UserSchema);