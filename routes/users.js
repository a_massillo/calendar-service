/**
@author: amassillo
**/
const express = require('express');
const { check, validationResult } = require('express-validator/check');
const router 	  = express.Router();

// Cargamos el controlador
const UserController = require('../controllers/users');

// Llamamos al router
//var md_auth = require('../middlewares/authenticated');
// Creamos una ruta para los métodos que tenemos en nuestros controladores
//router.get('/user/:id', md_auth.ensureAuth, UserController.getUser);

router.get('/:id',UserController.getUser);
//--------------------------------------------------------------------------------------------------
router.post('/', 
//express validator
[
  // username must be an email
  check('username').isEmail(),
  // password must be at least 5 chars long
  check('password').isLength({ min: 5 })
], 
UserController.saveUser);
//--------------------------------------------------------------------------------------------------

router.post('/asyn',UserController.getAsyncExample);
router.post('/:id/calendar',UserController.saveAgenda);
router.post('/:id/calendar/:calendarId/event',UserController.saveEvent);

router.get('/:id/calendar',UserController.getUserAgendas);
router.post('/:id/calendar/:calendarId/event/:eventId/comment',UserController.postCommentOnEvent);

module.exports = router;


