const mongoose = require('mongoose');

const connection = mongoose.connect(process.env.DB_URL,{ useNewUrlParser: true , useCreateIndex: true });

connection.then(db => {
		console.log('Successfully connected');
		return db;
	})
	.catch(err => {
			console.log('Error while attempting to connect to database:' + err);
	});

